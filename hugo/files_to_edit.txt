config.toml    //baseUrl and main menu points
layouts/partials/footer.html //Imprint
layouts/partials/header.html //SEO descriptions
content/profile.md //You can hijack this page as a template for static pages. If you need more than one, just make a copy
content/impressum.md //Update imprint
content/contact.md //Potentially update contact information
content/teaching/* //These can also be used as templates for this type of post in a directory/not directly on the front page. If you want to change the menu title, look at the config.toml
content/post/* //This is where ordinary news posts go.


There was an academics section on the website at some point. There were some listings for theses, which could potentially be a good option for the CRM website as well. I left the structure in, but there's no link to these offerings right now.