﻿+++
weight = 10
title="Vision of the new Master Program “Construction & Robotics”"
+++

{{<image image="/img/home/human-robot-interaction.jpg" opyright="IP, RWTH Aachen">}}

{{<inlinehead title="Vision">}}



In order to lay the groundwork for its long-term vision of the **construction site of the future**, the [Chair for Individualized Production](https://www.ip.rwth-aachen.de) at the Faculty of Architecture at RWTH Aachen University announces a new English-language Master’s programme in Construction Robotics (CR).  This programme will shape students in the innovative topics of digitalization and automation in the construction industry.  

The main objective of the programme is the development and use of **automated construction machinery and robotics** as the basis for **innovative construction processes** on construction sites. Our graduates will be the trailblazers and innovation scouts for our society to invent new design and work environments for the design and construction of quality-driven, low-cost housing, waste and emission reduced low CO2 construction sites. They will be able e.g. to develop new methods to build and inspect buildings and infrastructure, work out concepts for circular economy in the building sector, create minimal invasive construction scenarios for inner cities, develop new materials and sustainable construction technologies for buildings and infrastructure.

We are therefore looking for highly creative minds, open to the future with an interdisciplinary background and team in one of the top leading German Universities at RWTH Aachen.

The Master’s programme will combine physical prototyping of machinery and processes with virtual commissioning of construction sites and machinery in virtual simulation environments.

We believe, that only by combining the knowledge and expertise of different scientific fields, such as mechanical engineering, computer science, civil engineering and architecture, will we be able to comprehensively create a new digital construction environment and transform the means of constructing buildings. Therefore, the Chair of Individualized Production invites Bachelor’s graduates from all these academic backgrounds to come together in Aachen for a highly interdisciplinary study programme in the field of Construction Robotics.

Get a feeling for the spirit of the new CR Master’s programme by watching the video below!

<div style="text-align: center"><iframe src="https://player.vimeo.com/video/376897967?title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>


{{<inlinehead title="Motivation">}}


Why should you study Construction Robotics? Construction is one of the last industries with a low level of digitalization. Less than 6 % of construction companies utilize digital planning instruments beyond digital drawings. The construction industry is currently facing a profound transformation driven by an increase in available digitalization and automation technology. Furthermore, the construction industry of tomorrow will need to respond to several key social challenges, including the need to decrease the ecological footprint of construction by reducing construction waste, the emergence of smart cities, a demand for less invasive renovation processes, and the rapid aging of the population.

This growing demand of new digitally driven construction processes is not only argued by research and industry but also in society and politics. Since 2018, North Rhine-Westphalia as the first federal state of Germany has agreed in their coalition agreement to seize the opportunity of digitalization also in construction policy. 

There is a lot to do to reach the goal of a digital and automated construction environment, so come and join us at RWTH Aachen University as we combine the strengths of academia, science and industry.

{{<inlinehead title="Strategy">}}

{{<image image="/img/home/Strategy_1.jpg" opyright="IP, RWTH Aachen">}}


In order to design a digitalized and automated construction environment, students from different academic backgrounds will come together in the CR programme to work on the following tasks in seminars, lectures and especially in interdisciplinary project classes. 

The CR programme will guide you with theoretical, practical and methodological input to:

-	robotize or automate construction machinery
-	expand functionality of robots & construction machinery
-	make construction machinery controllable & programmable
-	develop adaptable programming methods
-	generate data management
-	implement a digital chain in construction processes
-	analyze and automate processes
-	invent new materials or production processes
-	develop new digital planning procedures

For more information on the content of the CR curriculum take a at the [curriculum](/curriculum)!

{{<inlinehead title="Graduate Profile">}}


{{<image image="/img/home/Graduate_Profile_1.jpg" opyright="IP, RWTH Aachen">}}
  

Upon graduating the CR Master’s programme, you will have an independent, highly specialized competence profile that complements the current spectrum profiles in the construction industry and opens up new innovation potential through its transdisciplinary approach. This will result in excellent career prospects. The following sectors will be the main objectives in this context:

-	construction production coordination
-	interface management of digital work processes
-	engineering, planning and consulting offices
-	consultanting for sustainable construction
-	project management or consulting for building companies
-	manufacturing of construction machines and equipment
-	public administration to develop guidelines and standards
-	prefabricating building components
-	construction industry

Graduates are also potentially qualified for scientific research, application-oriented product development, and as science managers at the interface of various disciplines. They will have a good understanding of interdisciplinary research and cooperation with researchers of different professional backgrounds.