+++
weight = 50
+++
{{<inlinehead title="Prof. Dr. Torsten Wolfgang Kuhlen">}}

{{<image image="/img/about/kuhlen.jpg" opyright="IP, RWTH Aachen">}}
**Virtual Reality & Immersive Visualization Group (Computer Science)**<br>
[The Virtual Reality & Immersive Visualization Group](https://www.vr.rwth-aachen.de/) is a member of the Visual Computing Institute as well as an integral part of the IT Center of RWTH. The group performs research in multimodal 3D interaction technology, immersive 3D visualization of complex simulation data, parallel visualization algorithms, and virtual product development. It is able to inject VR technology and methodology as a powerful tool into scientific and industrial workflows.
