+++
weight = 40
+++
{{<inlinehead title="Prof. Dr.-Ing. Markus Kuhnhenne">}}
{{<image image="/img/about/kuhnhenne.jpg" opyright="STB, RWTH Aachen">}}
**Institute for Steel Construction (Civil Engineering)**<br>
[The Institute of Steel Construction](http://www.stb.rwth-aachen.de/cms/STB/Die-Organisationseinheit/~mvqu/Lehr-und-Forschungsgebiet-Nachhaltigkei/?lidx=1) researches both steel construction as well as composite construction methods in interdisciplinary topics such as wind power, timber, and glass construction, as well as lightweight structures. The Institute’s emphasis lies on the development of innovative and multifunctional facade systems.