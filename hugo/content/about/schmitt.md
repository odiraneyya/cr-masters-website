+++
weight = 30
+++
{{<inlinehead title="Prof. Dr.-Ing. Robert Schmitt">}}
{{<image image="/img/about/schmitt.jpg" opyright="WZL, RWTH Aachen">}}
**Chair of Production Metrology and Quality Management at the Laboratory for Machine Tools and Production Engineering - WZL (Mechanical Engineering)**<br>
One focus for [Production Metrology and Quality Management](https://www.wzl.rwth-aachen.de/) lies in production integrated metrology and flexible automation of large scale assembly processes. Technical competence in the fields of positional measurements over large workspaces, computer vision, and sensor technologies is complemented with expertise regarding quality and innovation management systems.
