+++
weight = 100
+++
{{<inlinehead title="Infrastructure">}}
The CR Master’s programme benefits from both the scientific infrastructure of RWTH University´s powerhouse in general. To allow for physical prototyping, testing and evaluation, classes of the CR programme will take place in realistic lab environments such as the following: 

{{<image image="/img/about/Infrastructure.jpg" opyright="IP, RWTH Aachen">}}

- Small scale robotic lab with several low payload industrial robots and mobile platforms (Chair for Individualized Production, Faculty 2) 
- Large scale robotic lab with high payload industrial robots and overhead crane (Chair for Individualized Production, Faculty 2) 
- CIP-Pools with CAD/CAM/BIM Software (Faculty 2) 
- Wood-, Metal-, Plastics- and Photography Lab (Faculty 2) 
- Steel Manufacturing Lab (Institute of Steel Construction, Faculty 3) 
- GIS Software, Indoor positioning, incl. photogrammetry and laser scanning (Geodetic Institute, Faculty 3) 
- Centre for Metallic Design Lab (Faculty 3 and 4) 
- Robotic and Kinematics Lab (Institute of Mechanism Theory, Machine Dynamics and Robotics, Faculty 4) 
- Large Assembly and Production Metrology Lab (WZL, Research Area Production Metrology and Quality Management, Faculty 4) 

Furthermore, a reference construction site at the RWTH Aachen Campus West for teaching and research purposes is planned. Here, contact to realistic construction processes and actual heavy construction machinery in a dynamic development environment will be guaranteed.