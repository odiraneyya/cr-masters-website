+++
weight = 20
+++
{{<inlinehead title="Prof. Dr.-techn. Sigrid Brell-Cokcan">}}

{{<image image="/img/about/brell_cokcan.jpg" opyright="IP, RWTH Aachen">}}
**Chair for Individualized Production (Architecture)**<br>
[The Chair for Individualized Production in Architecture](https://www.ip.rwth-aachen.de/) researches intuitive systems for the use of construction robots; from the design and planning stages, through prefabrication and assembly to deconstruction. Furthermore, the Chair is also active in the fields of automation and technical consulting for construction processes.
