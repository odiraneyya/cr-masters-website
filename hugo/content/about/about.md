+++
weight = 10
+++
{{<image image="/img/about/wooden_arc.jpg" opyright="IP, RWTH Aachen">}}
{{<inlinehead title="Origin">}}

The new English-language CR Master’s programme is part of the teaching agenda at the [Architecture Faculty](https://arch.rwth-aachen.de/cms/~gfa/Architektur/?lidx=1) of RWTH Aachen University and is coordinated by the [ Chair for Individualized Production](https://ip.rwth-aachen.de) under the direction of Prof. Dr.-techn. Brell-Cokcan. Since 2015, Prof. Brell-Cokcan, her interdisciplinary team, and a network of industry and scientific partners have been researching into the digitalization and automation in construction with a focus on pre-fabrication and the construction site of the future. Furthermore, the Individualized Production team explores new technologies and working methods in interdisciplinary environments in daily business. Now, we want to share our scientific and professional skills with new students from all around the world in the context of construction and robotics!

{{<inlinehead title="Initiators">}}

The CR programme guarantees an interdisciplinary setup due to the diverse backgrounds of its initiators. Although the programme is hosted at the Faculty of Architecture, the initiating professors represent various faculties of RWTH Aachen University and address all academic fields of CR’s targeted student audience.
- Prof. Schmitt (Faculty of Mechanical Engineering)
- Prof. Kuhlen (Faculty of Computer Science)
- Prof. Kuhnhenne (Faculty of Civil Engineering)
- Prof. Brell-Cokcan (Faculty of Architecture)

The above listed faculty members have developed CR together and will further shape the structure of CR due to their strong commitment on the topic of digitalization and automation in the construction industry. These professors and more to come will enrich CR with their great scientific knowledge as well as their active support of students through supervision during lectures, seminars and projects.