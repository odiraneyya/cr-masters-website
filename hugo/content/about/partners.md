+++
weight = 110
+++
{{<inlinehead title="Industry Board">}}

We are happy to announce, that CR is supported by partnerships with construction industry leaders through an active collaboration with the [Center Construction Robotics (CCR)](https://construction-robotics.de/en/) in Aachen. The industry partners of the CCR draw upon their practical experience in business, in industry, and on the construction site to give input to ongoing projects. Furthermore, they contribute up to date research questions for seminars & projects, and share both profound insights and useful knowledge through the Construction Robotics lecture series.

**Center Construction Robotics (CCR)**<br>

[{{<image image="/img/about/CCR.png" opyright="IP, RWTH Aachen">}}](https://construction-robotics.de/)

Digital Automated Systems - Breaking digital boundaries together: The Center Construction Robotics (CCR) is closing digital gaps in the construction industry from planning through production to implementation. An interdisciplinary, international consortium of companies and faculty spanning many institutes of RWTH Aachen University are developing and designing intelligent processes, machines and business models along the entire construction value chain. The CCR jointly researches and develops key technologies in hardware, software and process design. Beyond indirect planning processes it aims to address, rethink, and redesign the entire construction site.