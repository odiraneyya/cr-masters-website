+++
weight = 40
title = "Lasers"
image = "/img/infrastructure/laser.bmp"
+++

We also got lasers.
You can sort these articles by adjusting the weight in the top section of this file.

Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
This example shows, how a link can be programmed: [This is a link](https://youtu.be/Bh7bYNAHXxw?t=23). You can also link to files residing in the /static/files directory  like so: [This is a link to a file](/files/generic_file.pdf)
