+++
image = "/img/infrastructure/bender.bmp"
weight = 10
title = "Robots"
+++

Yes, we have robots.

adipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.

This example shows, how a link can be programmed: [This is a link](https://youtu.be/oHg5SJYRHA0?t=43). You can also link to files residing in the /static/files directory  like so: [This is a link to a file](/files/generic_file.pdf)
