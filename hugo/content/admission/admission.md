+++
weight = 10
+++

{{<image image="/img/admission/Mood-Banner_5.jpg" opyright="IP, RWTH Aachen">}}

{{<inlinehead title="Admission requirements">}}

**General requirements**<br>
For the official requirements, links will soon be made available to an excerpt of the CR examination regulation as well as the complete documents from the RWTH Aachen University.  

In short, the admission requirements for the CR Master’s programme are as follows:

-	recognized first level university degree e.g. Bachelor degree
-	subject-specific educational background in architecture, civil engineering, mechanical engineering, computer science or equivalent educational programmes
-	proof of the required competencies comparable to those taught at RWTH Aachen University in relation to the subject-specific background (see details below)
-	sufficient proficiency in the English language
-	internship proving practical work experience in the construction industry for at least 40 days (can be proven latest during admission process for Master’s thesis)

**Subject specific competencies**<br>

You can apply for the CR Master’s programme if you can prove your competency in one of the four CR disciplines: architecture, civil engineering, mechanical engineering or computer science. Depending on the chosen discipline, consider one of the four documents below and check if the modules of your first level degree meet the RWTH demands listed in terms of content as well as credit points (CP):

-	[competencies in architecture](/files/Admission requirements_CR_architecture.pdf) / [2021](/files/Admission_requirements_CR_architecture_2021.pdf) 
-   [competencies in civil engineering](/files/Admission requirements_CR_civil eng.pdf) / [2021](/files/Admission_requirements_CR_civil_engineering_2021.pdf)
-	[competencies in mechanical engineering](/files/Admission requirements_CR_mechanical eng.pdf) / [2021](/files/Admission_requirements_CR_mechanical_engineering_2021.pdf)
-	[competencies in computer science](/files/Admission requirements_CR_computer sci.pdf) / [2021](/files/Admission_requirements_CR_computer_science_2021.pdf)

If you have a Bachelor’s degree in a different discipline than those listed, you can still apply to the CR programme as long as you can prove your competency in one of the disciplines.
{{<inlinehead title="Application procedure">}}


The application procedure as well as application deadlines are handled by the administration of RWTH Aachen University. Therefore, please consult the RWTH websites for details. Depending on your geographical background please refer to the different procedures for [EU/EEA citizens](http://www.rwth-aachen.de/cms/root/Studium/Vor-dem-Studium/Bewerbung-um-einen-Studienplatz/~dedx/Master-Bewerbung/?lidx=1) and [NON EU citizens](http://www.rwth-aachen.de/cms/root/Studium/Vor-dem-Studium/Bewerbung-um-einen-Studienplatz/Master-Bewerbung/~dqml/Bewerbung-Master-Internationale/?lidx=1) .

Unless stated otherwise by the RWTH, the application deadline for EU/EEA citizens for winter term is 15th of July and for summer term 15th of January. 

If not stated otherwise by the RWTH, the application deadline for NON EU citizens for winter term is 1st March and for summer term 1st of September.

{{<inlinehead title="Tuition and fees">}}

There are no tuition fees at RWTH Aachen University - this applies for international students as well. All students are, however, subject to a student body and therefore have to pay a social contribution fee.

The social contribution fee for Summer semester 2020 amounts to 291,48 euros. Each semester the fee amount slightly changes. Please find further information on current tuition fees and other cost estimates at the [RWTH University website here](https://www.rwth-aachen.de/cms/root/Studium/Vor-dem-Studium/Internationale-Studierende/Organisation-des-Studienaufenthaltes/Internationale-Studierende/~bqmo/Kosten/?lidx=1).

