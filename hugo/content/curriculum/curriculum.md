+++
weight = 10
+++
{{<image image="/img/curriculum/workshop.jpg" opyright="IP, RWTH Aachen">}}
{{<inlinehead title="Facts">}}

{{<image image="/img/curriculum/Curriculum_Fact.jpg">}}

{{<inlinehead title="Curriculum">}}

{{<image image="/img/curriculum/Curriculum 1.jpg">}}

The curriculum of the CR programme is divided into three major thematic columns which prepare for individual Master's Thesis. Each of the columns has a special focus which is explained below. The official CR study plan reflects this basic scheme and offers more detail on teaching modules. For more information on the study plan see section below. 

**Fundamentals of Construction & Robotics**<br>

Classes within the first column teach fundamental knowledge on current construction robotics as well as innovative process development. Knowledge is initially provided on a theoretical basis and later on complemented with practical input by the industrial board members and other invited lecturers.

**Discipline related classes for individual specialization**<br>

The second column allows for discipline related specialization in two ways. First of all, you will get to know basics of the other disciplines involved by choosing from a set of classes provided by the respective faculties. This will give you an understanding of the knowledge, methods and language of your classmates and team members. On the other hand, you will also specialize in your own discipline to enhance your knowledge. Again, you can choose from a set of classes by the respective faculty representing your own academic background. The taught expertise from your and the other disciplines can then be used to contribute to the team-based projects in the last column.

**Interdisciplinary team projects**<br>

Within the last column, students transfer the knowledge they have gained into scientific and practical development through interdisciplinary, team-based projects. To begin with, the projects are meant to practice general topics of automated processes and interdisciplinary working procedures. Then, the project teams will train scientific methods to evaluate up-to-date research potentials, formulate research questions and develop their own strategies to prepare for solving the identified research question. Finally, the pursed research questions and innovative process ideas will be practically examined due to a prototyping project on a 1:1 scale. The focus of the projects and prototypes can vary between software, hardware, processes and construction elements according to the team’s interests, the provided input of the industrial board or current research topics.

**Master's Thesis**<br>

As a final module to prepare for graduation, each student will write an individual Master’s Thesis combining all knowledge gained earlier in the Master’s programme. Similar to the team projects, the CR team as well as the industry board will collect current research questions for the Master’s thesis with focus on relevant topics for digitalization and automation of the construction industry. 

{{<inlinehead title="Study Plan">}}

The curriculum of CR is based on the study plan displayed below. The study plan suggests the recommended order of classes spread over the four semesters of the Master’s degree. Depending on the start time you choose, the recommended order of classes may vary slightly. 
[{{<image image="/img/curriculum/StudySchedule_WinterTerm.jpg">}}](/files/Studienverlauf_CR_v20_WiSe_3_.pdf)
[{{<image image="/img/curriculum/StudySchedule_SummerTerm.jpg">}}](/files/Studienverlauf_CR_v20_SoSe_3_.pdf)

For more information on the study plan see the official examination regulation as well as the module catalog on the website of [the Architecture Faculty of RWTH Aachen University](https://arch.rwth-aachen.de/cms/Architektur/Studium/Studiengaenge/~ffuge/M-Sc-Construction-and-Robotics/). 
