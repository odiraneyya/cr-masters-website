+++
weight = 5
+++

<a id="m1_iot_2020"></a>
{{<inlinehead title="Interdisciplinary, design-driven projects">}}

<div align="center">
	<img alt="IoT connectivity on the construction site" 
	src="/img/curriculum/m1_interdesciplinary_project.jpg" />
</div>

## Scope:

The Internet of Things (IoT) refers to the evolution of all machines when they begin to communicate amongst themselves and with other types of machinery. In this series of projects, we would like to explore this idea in the scope of construction. We are very excited about these interdisciplinary projects, as we believe they will provide our students with various opportunities to work on exciting and novel IoT applications in the field of construction.

In our interdisciplinary, design-driven projects, we will offer 3 different projects to work towards the vision “Connect our construction site”:

- Project A: Collecting machine parameters
- Project B: Virtual model of construction sites
- Project C: Efficient networking on construction sites

After having a short introduction to each project all together, the students will be first assigned to work on one of the projects. Advanced introductions will be offered in different time slots, as presented in the time schedule. The students only have to take part in their own advanced introductions. However, the students can participate in other advanced introduction for their own interest. Specific objectives for each project will be individually defined after the final assignment. 

More detailed information will be provided on Tuesday 07.04. in our digital classroom.  

Due to the Coronavirus crisis, the whole course will take place in the digital learning room. Access data will be given to the students individually.

## Schedule:

<table width="100%">
<tr><td>Tu 07.04.2020<td>10:00 – 11:30<td>digital<td>Introduction to projects</tr>
<tr><td colspan="4" style="padding: 12px; text-align: center; background-color: #EBEBEB"><b>Initial project assignment</b></tr>
<tr><td>Tu 14.04.2020<td>10:00 – 11:30<td>digital<td>Advanced Introduction to project A</tr>
<tr><td>Tu 14.04.2020<td>12:00 – 13:30<td>digital<td>Advanced Introduction to project B</tr>
<tr><td>Tu 14.04.2020<td>14:00 – 15:30<td>digital<td>Advanced Introduction to project C</tr>
<tr><td>&nbsp;</tr>
<tr><td>Tu 21.04.2020<td>10:00 – 11:30<td>digital<td>Advanced Introduction to project A</tr>
<tr><td>Tu 21.04.2020<td>12:00 – 13:30<td>digital<td>Advanced Introduction to project B</tr>
<tr><td>Tu 21.04.2020<td>14:00 – 15:30<td>digital<td>Advanced  Introduction to project C</tr>
<tr><td colspan="4" style="padding: 12px; text-align: center; background-color: #EBEBEB"><b>Refined project assignment</b></tr>
<tr><td>Every Tuesday	<td>10:00 – 11:30<td>digital<td>Mentoring</tr>
<tr><td>TBD				<td>10:00 – 11:30<td>digital<td>Colloquium (mid-term)</tr>
<tr><td>Fr 07.07.2020	<td>10:00 – 11:30<td>digital<td>Digital final presentation</tr>
</table>