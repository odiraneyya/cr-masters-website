+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/christian_raabe.png"
type = "rwth_person"
weight = 84
title= "Univ.-Prof. Dr.-Ing.<br />Christian Raabe"
+++

#### Univ.-Prof. Dr.-Ing. Christian Raabe   

Department for Historic Building Conservation and Research (DHB),  
Faculty 2 of Architecture  
RWTH Aachen University

Website: [http://www.dhb.rwth-aachen.de](http://www.dhb.rwth-aachen.de)  
Email: [raabe@dhb.rwth-aachen.de](mailto:raabe@dhb.rwth-aachen.de)

The topic of Planning and Building in Existing Contexts is becoming an increasingly important requirement in a society that is aware of its limited resources. The need for more extensive training in this area is based on the knowledge that future architects will, more than ever before, have to deal with existing structures and their repurposing, restructuring and expansion. Sustainable urban development and sustainable urban planning require different approaches compared to new construction projects.  

Historic buildings are a limited social resource. Historic monuments make up only a small percentage, around 3 percentage, in this area, but they make a significant contribution to connecting the past to the present and making history tangible and visible. In contrast to the ever more extensive virtual experiences, they represent the value of the original and the authentic. Each building has its own individual story and every project poses a unique challenge to the architect. These cannot be handled alone but in association with specialist: conservationists, building researchers, restorers, scientists, engineers and architects. 

**Competencies:**

- Methods of construction analysis and documentation
- Evaluation criteria for historical constructions and building materials
- Concepts for conservation and climate protection
- Restoration and upgrading technologies 



