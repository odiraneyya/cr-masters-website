+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/karina_schiefer.png"
type = "other_person"
weight = 111
title= "DI Karina Schiefer "
+++

#### DI Karina Schiefer 

Business Excellence/Digital Unit  
Project Manager Digital Innovations  
![](/img/people/porr_icon.png)

Website: [porr-group.com/en/](https://porr-group.com/en/)  
Email: [karina.schiefer@porr.at](mailto:karina.schiefer@porr.at)

We have a motto for the way we do business at PORR: intelligent building connects people. Our goal is to bring together our high standards regarding quality, technology and efficiency with every person involved in a project – whether a builder or a user. After all, we build with and for people. Values such as trustworthiness, reliability, customer focus and teamwork are an important part of our identity, as is an approach based on partnerships.  

PORR is part of the Master Programme Construction Robotics and of the Center Construction Robotics because we want to find new ways of boosting the development of research and teaching scenarios aimed at training the best minds in the construction industry. This is the only way we will be able to meet challenges together - not just in Germany, but all over the world.  

In addition to the exchange of practical experience from the construction sector, the aim of the course is to develop different approaches for the successful implementation of interdisciplinary projects.

**Lectures:**  

- Soft Skills for interdisciplinary project development