+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/marzia_traverso.jpg"
type = "rwth_person"
weight = 108
title= "Univ.-Prof. Dr.-Ing.<br />Marzia Traverso"
+++

#### Univ.-Prof. Dr.-Ing. Marzia Traverso 

Head of the Institute of Sustainability in Civil Engineering (INaB),  
Faculty 3 of Civil Engineering  
RWTH Aachen University 

Website: [http://www.inab.rwth-aachen.de/](http://www.inab.rwth-aachen.de/?page_id=102&lang=en)  
Email: [marzia.traverso@inab.rwth-aachen.de](mailto:marzia.traverso@inab.rwth-aachen.de)

While the protection of our planet and its sustainable development are on the political agenda, its implementation remains a major challenge in daily practice. Both lectures present methods and tool to assess environmental and sustainability in its three dimensions of companies, projects, products and services, where the sustainability assessment methods and tools give an overview of those methodologies with practical examples and implementation.  

Considering the three dimensions of the sustainability (economic, social and environmental one), the second lecture Lifa cycle Consolidation focuses the attention on the environmental assessment of products and services according to the norm ISO 1404x series. It presents in detailed the different methods to perform a Life cycle assessment of a product, and the different methodologies and initiative to support decision making processes and developers towards a more sustainable production. Furthermore, in accordance with the 12 Sustainable Development Goal – Agenda 2030- particular attention is posed to the presentation and communication of the result to support a sustainable consumption. 


**Lectures:**  

- Civil Engineering Basics:
  - Sustainability Assessment - Methods and Tools
  - Life Cycle Assessment - Consolidation



