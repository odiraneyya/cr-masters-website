+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/verena_nitsch.jpg"
type = "rwth_person"
weight = 72
title= "Univ.-Prof. Dr.-Ing.<br />Verena Nitsch"
+++

#### Univ.-Prof. Dr.-Ing. Verena Nitsch 

Director of Institute of Industrial Engineering and Ergonomics (IAW),  
Faculty 4 of Mechanical Engineering  
RWTH Aachen University 

Website: [http://iaw.rwth-aachen.de](http://www.iaw.rwth-aachen.de)  
Email: [v.nitsch@iaw.rwth-aachen.de](mailto:v.nitsch@iaw.rwth-aachen.de)

The lecture Industrial Engineering and Ergonomics serves as an introduction into the topic of employee centered workplace design. Within the course the students deal with the issues of human factors related challenges and solutions in modern industrial enterprises. At the same time, the lecture prepares them for the application of according methods, tools and standards in industrial enterprises and encourages them to apply their acquired knowledge to shape the future of work.  

The latest findings from applied research projects at the IAW are always included in the lectures – providing the students with the most up to date information and best practice examples.

**Lectures:**  

- Innovation and Production Management:
  - Industrial Engineering



