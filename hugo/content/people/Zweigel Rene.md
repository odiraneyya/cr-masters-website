+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/rene_zweigel.png"
type = "rwth_person"
weight = 112
title= "Dr.-Ing. René Zweigel"
+++

#### Dr.-Ing. René Zweigel  

Managing Director of Institute of Automatic Control (IRT),  
Faculty 4 of Mechanical Engineering  
RWTH Aachen University


Website: [https://irt.rwth-aachen.de](https://irt.rwth-aachen.de)  
Email: [r.zweigel@irt.rwth-aachen.de](mailto:r.zweigel@irt.rwth-aachen.de)

The Institute of Automatic Control, headed by Prof. Dirk Abel, is located in the Faculty 4 of Mechanical Engineering at RWTH Aachen University and is an intermediary among control theory and diverse application fields of control and automation technologies.  

In addition to basic control theory, the main focus of research work is on mechanical engineering, automotive engineering, process engineering, energy and medical technology. Furthermore, the institute has long term experience in multi-constellation and dual-frequency aided inertial-navigation systems for different applications, such as automotive, railway, aviation, and shipping. The institute was responsible for the construction of Galileo test facilities for railway and automobile applications near Aachen. Therefore, the development of navigation filters using GPS and the new European satellite-navigation system Galileo is also part of the institute’s expertise and research activity.

**Lectures:**  

- Construction Robotics Lecture:
  - Navigation solutions for autonomous vehicles


