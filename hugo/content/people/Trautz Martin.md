+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/martin_trautz.png"
image_caption = "/img/people/trako_rwth.png"
type = "rwth_person"
weight = 106
title= "Univ.-Prof. Dr.-Ing.<br />Martin Trautz"
+++

#### Univ.-Prof. Dr.-Ing. Martin Trautz

Director of the Chair of Structures and Structural Design (TRAKO),    
Faculty 2 of Architecture  
RWTH Aachen University   

Website: [https://trako.arch.rwth-aachen.de](https://trako.arch.rwth-aachen.de/cms/~hpns/TRAKO/lidx/1/)  
Email: [trautz@trako.arch.rwth-aachen.de](mailto:trautz@trako.arch.rwth-aachen.de)

At the Chair of Structures and Structural Design, this particular aspect of the environment, namely the effect of forces and strains on any object, including buildings, is the focal point of consideration. This task is closely linked to architectural design since the structure is oftentimes an integral and visible part of the architecture, and, in certain cases like bridges or civil engineering works, constitutes the design as a whole. In order to rise to this interdisciplinary challenge, our team consist of architects as well as civil engineers.

The basic knowledge and principles of a structural design approach as well as the development and planning of supporting structures are being conveyed as part of the undergraduate curriculum via lectures, tutorial, seminars and design projects. On this basis, the graduate courses incorporate current research topics and offer students a possibility to engage in relevant design projects. The seminars focus on the development of innovative projects up to a ready for construction stage. Topics for design projects and seminars are derived from current research focus of the chair, for instance in the field of timber constructions, industrial planning and infrastructure, or structural morphology principles like folding.



**Lectures:**  

- Architecture Advanced: 
  - CR_ Trako (Interdisziplinäre Fabrikplanung)