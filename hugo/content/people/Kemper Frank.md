+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/frank_kemper.png"
type = "rwth_person"
weight = 54
title= "Univ.-Prof. Dr.-Ing.<br />Frank Kemper"

+++

#### Univ.-Prof. Dr.-Ing. Frank Kemper

Institute of Steel Construction (STB),  
Faculty 3 of Civil Engineering  
RWTH Aachen University  

Website: [https://stb.rwth-aachen.de/en](https://www.rwth-aachen.de/cms/~a/root/?lidx=1)  
Email: [kemper@stb.rwth-aachen.de](mailto:kemper@stb.rwth-aachen.de)

The institute of steel construction has a long history in research and teaching. In the academic training of civil engineers, we set a high priority on giving students the fundamental knowledge base and theoretical background for the assessment of complex problems in their future fields of activity. At the same time, we strive to close the gap between academics and practice by discussing application-oriented topics and providing contact to practitioners. Within the course “Steel Structures in Industrial Applications”, basic theories of mechanics are applied and supplemented by more advanced topics such as stability or 2nd order theory. 

**Lectures:**  

- Construction Robotics Lecture:
  - Steel Structures in Industrial Applications

