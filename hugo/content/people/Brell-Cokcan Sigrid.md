+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/sigrid_brellcokcan.png"
weight = 1
type = "rwth_person"
title= "Univ.-Prof. Dr. techn. Sigrid Brell-Cokcan"
+++

#### Univ.-Prof. Dr. techn. Sigrid Brell-Cokcan

Director of the Chair for Individualized<br />Production (IP),  
Faculty 2 of Architecture  
RWTH Aachen University

Website: [https://www.ip.rwth-aachen.de/](https://www.ip.rwth-aachen.de/)  
Email: [brell-cokcan@ip.rwth-aachen.de](mailto:brell-cokcan@ip.rwth-aachen.de)

The Chair of Individualized Production (IP) focusses on the use of innovative machinery in material and building production. In order to create an environment that allows the efficient, individualized production of lot size one, new and user friendly methods for man machine interaction are developed. IP employs researchers from different fields of robotics and building production to streamline the necessary digital workflow from the initial design to the production process; shaping the construction site of the future via intuitive, easy-to-use interfaces.  

In 2018 Sigrid Brell-Cokcan co-founded the cross-faculty Center for [Construction Robotics (CCR)](https://www.construction-robotics.de) on RWTH Aachen Campus to focus on the automation in construction with key industry leaders along the construction industry value chain.  

In 2020 together with an interdisciplinary RWTH team across four faculties, the new international Master Degree Construction & Robotics (CR) and the Reference Construction Site on Campus West was launched, which will serve as a living lab for our interdisciplinary student body throughout their studies.  

**Lectures:**  

- Construction Robotics Lecture Series
- Design Driven Project
- Research Driven Project
- Prototyping Project

 
 
