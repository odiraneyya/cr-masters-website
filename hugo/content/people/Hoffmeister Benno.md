+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/benno_hoffmeister.jpg"
type = "rwth_person"
weight = 48
title= "Univ.-Prof. Dr.-Ing.<br />Benno Hoffmeister"
+++

#### Univ.-Prof. Dr.-Ing. Benno Hoffmeister  

Head of Div. of Timber Structures at Institute of Steel Construction (STB),  
Faculty 3 of Civil Engineering  
RWTH Aachen University 

Website: [https://www.stb.rwth-aachen.de/en](https://www.stb.rwth-aachen.de/en)  
Email: [hoff@stb.rwth-aachen.de](mailto:hoff@stb.rwth-aachen.de)

Timber as building material has become very popular in the recent past. The continuously increasing popularity is linked to the fact that wood is a renewable material with a very good CO2 balance on one hand and to the increasing availability of engineered wood materials providing tailored mechanical properties in combination with highest precision and quality of the final products. The increasing market shares of timber structures result in the demand for qualified engineers and designers understanding and capable of utilising the particular characteristics of this material in the context of structural engineering.  

The lecture Timber Structures I provides an introduction into the material properties of wood and into the design concept of timber structures. Within the lecture the principles of structural members, fasteners and joints and the related verification methods are explained. The lectures comprise also the design of roof structures, stability in compression as well as aspects of serviceability and durability.

**Lectures:**  

- Civil Engineering Basics:
  - Timber Structures I

