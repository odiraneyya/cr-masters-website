+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/markus_oeser.png"
image_caption = "/img/people/isac_rwth.png"
type = "rwth_person"
weight = 76
title = "Univ.-Prof. Dr.-Ing. <br />Markus Oeser "
+++

#### Univ.-Prof. Dr.-Ing. Markus Oeser   

Director of Chair of Highway Engineering (isac),   
Faculty 3 of Civil Engineering  
RWTH Aachen University    

Website: [https://www.isac.rwth-aachen.de/](https://www.isac.rwth-aachen.de)  
Email: [oeser@isac.rwth-aachen.de](mailto:oeser@isac.rwth-aachen.de)

A high-quality compaction of the asphalt mixture is of great importance for the proper design and construction of high performance asphalt pavements. In order to improve the performance of asphalt, the compaction process of the hot asphalt mix is numerically simulated. The flow of material during the compaction under the paver screed is simulated by discrete element method (DEM). In addition, an intellectual measurement was applied to monitor the movement of granular material during paver compaction, the movement or kinematic properties of material during paving and the contact force condition of paving materials can be detected. After the paving, the asphalt performance is simulated by finite element method (FEM) using the generated micro-structure of the asphalt mixtures from X-ray Computer Tomography (X-ray CT). Different compaction methods and material properties of the asphalt mixture components are applied to investigate their influence on the performance of the asphalt mixtures. This study will help with forming the currently missing theoretical framework to optimize in-situ compaction of asphalt pavement. Meanwhile, it can also be used for guiding the construction of better asphalt compaction equipment (pavers, rollers) and thus ensure the transfer of the acquired basic knowledge into application-oriented research and development.   

**Lectures:**

- Construction Robotics Lecture
- Civil Engineering Basics:
  - Pavement Dynamics
- Civil Engineering Advanced:
  - Mobility Research and Transport Modeling
