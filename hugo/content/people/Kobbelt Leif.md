+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/leif_kobbelt.png"
type = "rwth_person"
weight = 58
title= "Univ.-Prof. Dr. Leif Kobbelt"

+++

#### Univ.-Prof. Dr. Leif Kobbelt

Director of Chair of Computer Science 8 (Computer Graphics and Multimedia),   
Faculty 1 of Mathematics, Computer Science Department  
RWTH Aachen University    

Website: [https://www.graphics.rwth-aachen.de/](https://www.graphics.rwth-aachen.de/)  
Email: [sekretariati8@cs.rwth-aachen.de](mailto:sekretariati8@cs.rwth-aachen.de)

The Chair of Computer Graphics and Multimedia is part of the Visual Computing Institute (VCI) which is a dedicated research institute within the Computer Science Department at RWTH Aachen University. It brings together research groups that are addressing the diverse scientific aspects of the generation, processing, analysis, and display of all kinds of visual data (including images, videos, as well as 3D models and scenes).  

As such, the VCI covers a very broad spectrum of research competences ranging from geometry processing and computer graphics to computer vision, immersive visualization and simulation. Our mission is to develop innovative methods and algorithms in this area and to create impact in fundamental research as well as in applications in computational engineering, CAD/CAM, and digital entertainment.  


**Lectures:**  

- Computer Science Basics:
  - Basic Techniques in Computer Graphics
