+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/frank_piller.jpg"
type = "rwth_person"
weight = 80
title= "Univ.-Prof. Dr. rer. pol.<br />Frank Thomas Piller"
+++

#### Univ.-Prof. Dr. rer. pol. Frank Thomas Piller  

Professor of management and Head of Institute for Technology and Innovation Management (TIM),  
Faculty 8 of Business and Economies   
RWTH Aachen University

Website: [https://www.time.rwth-aachen.de/.../](https://www.time.rwth-aachen.de/cms/TIME/Die-Research-Area/~zzby/Uebersicht/lidx/1/)  
Email: [piller@time.rwth-aachen.de](mailto:piller@time.rwth-aachen.de)

Frank Piller is a professor of management and Head of Institute for Technology and Innovation Management (TIM) at RWTH Aachen University, Germany, the center of "German engineering". At RWTH Aachen, he currently serves as the Dean the RWTH Business School. Before entering his recent position in Aachen, he worked at the MIT Sloan School of Management (2004-2007) and TUM School of Management, Technische Universitaet Muenchen. He held visiting professorships at HKUST, Chalmers University, and IE Business School.  

Prof. Piller is regarded as one of the leading experts on strategies for customer-centric value creation, like mass customization, personalization, and innovation co-creation. His group at RWTH became one of the globally leading research centers for open and distributed innovation, supported by competitive research grants of about US$1million annually. As indicated by a h-Score of 54, >15,000 citations of his publications (Google Scholar), and more than 250 general press articles about his studies (and listed as the only German professor on LDRLB's "Top50 Profs on Twitter" list), his work has attracted broad attention and reception in academia and the management community.


**Lectures:**  

- Innovation and Production Management:
  - Managing the Innovation Process: Online Format
  - Strategic Technology Management
