+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/ethan_kerber.png"
type = "rwth_person"
weight = 56
title= "Ethan Kerber"

+++

#### Ethan Kerber

Researcher at the Chair for Individualized Production (IP),  
Faculty 2 of Architecture  
RWTH Aachen University   

Website: [https://www.ip.rwth-aachen.de/](https://www.ip.rwth-aachen.de/)  
Email: [kerber@ip.rwth-aachen.de](mailto:kerber@ip.rwth-aachen.de)

Ethan Kerber is a computational designer working at the intersection of Architecture, Engineering and Construction. Ethan has a Master’s degree in Industrial Design from the San Francisco State University and a Master of Engineering in Computational Design and Construction from the Hochschule Ostwestfalen-Lippe. He has experience in metalwork and digital fabrication with public sculptures permanently installed in Washington DC and Arlington Virginia. Ethan is currently a researcher at The Chair of Individualized Production where he is developing a doctoral dissertation on the Optimization and Automation of Steel Structures.  

The Chair of Individualized Production (IP) founded by Prof. Sigrid Brell-Cokcan in 2015 focusses on the use of innovative machinery in material and building production. In order to create an environment that allows the efficient, individualized production of lot size one, new and user friendly methods for man machine interaction are developed. IP employs researchers from different fields of robotics and building production to streamline the necessary digital workflow from the initial design to the production process; shaping the construction site of the future via intuitive, easy-to-use interfaces.

**Lectures:**  

- Digital and Soft Skills for Interdisciplinary Project Work
- Architecture Basics:
  - CR_Visual Programming Advanced
  - CR_IGP2-IP: Individualized Construction
