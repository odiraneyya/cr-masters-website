+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/marco_hutter.png"
image_caption = "/img/people/rsl_eth.png"
type = "other_person"
weight = 52
title = "Univ.-Prof. Dr.<br />Marco Hutter"
+++

#### Univ.-Prof. Dr. Marco Hutter  

Director of Robotic Systems Lab,  
ETH Zurich

Website: [https://www.rsl.ethz.ch](https://www.rsl.ethz.ch/)  
Email: [mahutter@ethz.ch](mailto:mahutter@ethz.ch)

The Robotic Systems Lab develops machines and their intelligence to operate autonomously in rough and challenging environments. With a large focus on robots with arms and legs (e.g. quadrupedal robots, mobile manipulators, hybrid wheel-leg systems), the research includes novel actuation methods for advanced dynamic interaction, innovative designs for increased system mobility and versatility, and new control, optimization and learning algorithms for locomotion and manipulation. Our machines are deployed in various real-world scenarios, such as for exploration and inspection of offshore industrial sites, mines, sewer, tunnels, or caves, for search and rescue operations, in rehabilitation and for construction.

Since 2015, Marco Hutter is Principal Investigator of the NCCR digital fabrication, a national competence centre for research in digital fabrication in Switzerland. As part of this large-scale research program, interdisciplinary collaborators from robotics, architecture, and civil engineering investigate new ways of using robot technologies to facilitate new building processes. This ranges from AR to support humans on construction sites, over small mobile robots that can conduct high-precision working tasks on site, to automated construction machines.


**Lectures:**  

- Construction Robotics Lecture Series
