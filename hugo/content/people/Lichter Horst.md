+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/horst_lichter.png"
type = "rwth_person"
weight = 66
title= "Univ.-Prof. Dr. rer.nat.<br />Horst Lichter"
+++

#### Univ.-Prof. Dr. rer.nat. Horst Lichter

Head of Research Group Software Construction (SWC),  
Faculty 1 of Mathematics, Computer Science Department  
RWTH Aachen University 

Website: [https://www.swc.rwth-aachen.de](https://www.swc.rwth-aachen.de/)  
Email: [lichter@swc.rwth-aachen.de](mailto:lichter@swc.rwth-aachen.de)

Software in an integral part of almost all innovative products. Furthermore, software is an important tool to develop and operator products. Software development is a complex and challenging tasks where many different skills are needed to create the right system fulfilling the desired requirements. One crucial discipline needed to develop high quality software is identifying the problem domain and designing the technical software architecture, as the architecture has many positive or negative impacts on the final software system.  

Another aspect, which becomes more and more important, is software quality, as software sometimes takes over decisions and is controlling critical processes or operations. Therefore, the research group Software Construction is focusing on developing new methods, tools and techniques to support software engineers.

**Lectures:**  

- Computer Science Advanced:
  - Object-oriented Software Construction