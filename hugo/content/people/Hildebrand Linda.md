+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/linda_hildebrand.png"
type = "rwth_person"
weight = 44
title= "Junior Professor, Dr.-Ing. Linda Hildebrand"
+++

#### Junior Professor, Dr.-Ing. Linda Hildebrand  

Director of the Chair for Cycle Oriented Construction (RB),  
Faculty 2 of Architecture  
RWTH Aachen University  

Website: [https://www.rb.rwth-aachen.de/.../](https://www.rb.rwth-aachen.de/cms/~hpbo/RB/)  
Email: [lhildebrand@rb.arch.rwth-aachen.de](mailto:lhildebrand@rb.arch.rwth-aachen.de)

Linda Hildebrand is a junior professor for Cycle Oriented Construction at the Faculty of Architecture at RWTH Aachen. At TU Delft, she did her doctorate on the topic of life cycle assessments in the architecture planning process. Besides, she worked on methods and implementation of circular economy in construction in research and teaching. She is an architect with a focus on ecological consultancy.

**Lectures:**  

- Construction Robotics Lecture:
  - Using digital planning tools and automation for Circularity


