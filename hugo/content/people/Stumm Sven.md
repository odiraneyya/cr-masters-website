+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/sven_stumm.png"
type = "rwth_person"
weight = 104
title= "Dr.-Ing. Sven Stumm"
+++

#### Dr.-Ing. Sven Stumm

Senior Researcher at the Chair for Individualized Production (IP),  
Faculty 2 of Architecture  
RWTH Aachen University   

Website: [https://www.ip.rwth-aachen.de/](https://www.ip.rwth-aachen.de/)  
Email: [stumm@ip.rwth-aachen.de](mailto:stumm@ip.rwth-aachen.de)

Sven Stumm is a computer scientist with extensive experience in electrical and mechanical engineering. He is currently leading the robotic programming and outdoor robotics research team at the chair for Individualized Production in architecture at RWTH Aachen University. Sven Stumm submitted the first PHD Thesis at the Chair for Individualized Production in Architecture (IP) in 2018 titled "Interconnecting Design Knowledge and Construction by Utilizing Adaptability and Configurability in Robotics", which was graded summa cum laude.  
 
His research focusses on accessibility of robotics, through human robot collaboration and sensor based adaption with a-priori knowledge. His work is based on 14 years of experience in software design and development ranging from optimization and intelligent system design over simulation software to data standardization and exchange between heterogeneous software tools, as well as extension of computer aided design software. 10 years of experience in robotics ranging from mobile humanoid robotics, over mobile service robotics to industrial and mobile industrial robotics, as well as 7 years of experience in scientific research and teaching as a scientific assistant at TU Dortmund University and at the RWTH Aachen University. His work allowed him to gain 3 years of experience in mechanical engineering and industrial assembly processes, as well as 4 years of experience in architecture and construction processes.


**Lectures:**  

- Construction Robotics Lecture