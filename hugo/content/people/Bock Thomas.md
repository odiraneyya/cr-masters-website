+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/thomas_bock.png"
type = "other_person"
weight = 32
title= "Univ.-Prof. Dr.-Ing.<br />Thomas Bock"
+++

#### Univ.-Prof. Dr.-Ing. Thomas Bock  

Director of the Chair for Building Realization and Robotics (br2),  
Faculty of Architecture  
Technical University of Munich (TUM)

Website: [http://www.br2.ar.tum.de](http://www.br2.ar.tum.de/)  
Email: [thomas.bock@br2.ar.tum.de](mailto:thomas.bock@br2.ar.tum.de)

Introduction: Mission of the chair for Building Realization and Robotics is to extend the traditional core competences of design and build, broadening the activity area of future graduates, professionals and creating new employment opportunities. Located at TUM within the Bavarian high tech cluster -in which the chair is well connected - the chair functions as an incubator for the development and socio-technically integrated and building related technologies.  

In the Master Course Advanced Construction and Building Technology which the chair is coordinating since 2011 the chair has achieved to concentrate students coming from 8 different professional backgrounds (Architecture, Industrial Engineering, Electrical Engineering, Civil Engineering, Business Science, Interior Design, Informatics, Mechanical Engineering).

**Lectures:**  

- Construction Robotics Lecture

