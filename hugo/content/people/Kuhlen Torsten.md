+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/torsten_kuhlen.jpg"
type = "rwth_person"
weight = 60
title= "Univ.-Prof. Dr.<br />Torsten Wolfgang Kuhlen"
+++

#### Univ.-Prof. Dr. Torsten Wolfgang Kuhlen

Professor for Virtual Reality,  
Faculty 1 of Mathematics, Computer Science Department  
RWTH Aachen University  

Website: [https://www.vr.rwth-aachen.de](https://www.vr.rwth-aachen.de)  
Email: [kuhlen@vr.rwth-aachen.de](mailto:kuhlen@vr.rwth-aachen.de)

Our research interests include all areas of Virtual Reality with a focus on immersive data analysis in scientific and technical applications as well as the design and evaluation of 3D, multimodal human computer interfaces. Application domains comprise production technology, architecture, robotics, simulation science, medicine, and neuroscience. As an integral part of the RWTH IT Center, we operate the aixCAVE, one of the largest VR installations worldwide, and we provide services in Virtual Reality and Visualization to science and industry.

**Lectures:**  

- Computer Science Basics:
  - Virtual Reality
  - Advanced Topics of Virtual Reality

