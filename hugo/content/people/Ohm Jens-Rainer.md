+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/jens-rainer_ohm.png"
image_caption = "/img/people/ient_rwth.png"
type = "rwth_person"
weight = 78
title = "Univ.-Prof. Dr.-Ing.<br />Jens-Rainer Ohm"
+++

#### Univ.- Prof. Dr.-Ing. Jens-Rainer Ohm 

Head, Institute for Communications Engineering 
Faculty 1 of Mathematics, Computer Science Department  
RWTH Aachen University

Website: [http://ient.rwth-aachen.de](http://www.ient.rwth-aachen.de/cms/)  
Email: [ohm@ient.rwth-aachen.de](mailto:ohm@ient.rwth-aachen.de)

Our focused research topics are in the area of information compression and signal analysis, with emphasis on multimedia data, and more recently, biological data.  

In Video Coding, our research interests include future higher-compression coding algorithms, neural networks and machine learning approaches for compression, scalable coding, and coding based on 3D scene structures as well as video panoramas. In Signal Analysis, we work on deep learning approaches for efficient indexing and identification of imagery, scenes and objects, e.g. for retrieval applications, and for nonlinear prediction of irregular time series and events. The research interests in biological signal processing are focusing on compression and analysis of signals expressing genomic information.  

We frequently contribute to the major conferences dedicated to our area. The research work is framed by long-term involvement in standardization activities and by projects with industry and academic partners all over the world. In this context, we continue contributing to compression standards like Advanced Video Coding, High Efficiency Video Coding, and Versatile Video Coding.
For this lecture students shall acquire an advanced knowledge about signal processing, time/frequency characterization, sampling, estimation and detection problems with emphasis on application in communication systems signal analysis and systems optimization.  	


**Lectures:**

- Computer Science Advanced:
  - Advanced Topics in Signal Processing and Communication

