+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/robert_schmitt.png"
type = "rwth_person"
weight = 99
title= "Univ.-Prof. Dr.-Ing.<br />Robert H. Schmitt"
+++

#### Univ.-Prof. Dr.-Ing. Robert H. Schmitt

Director of Chair of Production Metrology and Quality Management,  
Faculty 4 of Mechanical Engineering  
RWTH Aachen University  

Website: [https://wzl.rwth-aachen.de](https://www.wzl.rwth-aachen.de/cms/WZL/Forschung/~sujg/Fertigungsmesstechnik/?lidx=1)  
Email: [r.schmitt@wzl.rwth-aachen.de](mailto:r.schmitt@wzl.rwth-aachen.de)

**Lecture Robotic Sensor Systems**  
The number of robotic applications in our everyday life increases continuously. In both private households and industrial environments, we observe a constant diversification of operational areas of robots. While the same type of robot is used in different applications (e.g. an industrial robot for welding or palletizing), the sensors such as cameras or force sensors attached to the robotic system differ depending on process requirements. Thus, the robotic system becomes highly adaptive by the use of additional sensor equipment.
The lecture Robotic Sensor Systems provides an understanding of industrial robots as metrological systems in industrial processes. It will be explained how a robotic system can be extended by sensors to meet specific process requirements. Current sensor technologies are presented and underlying physical fundamentals are explained.  For the use of the obtained sensor data, techniques for signal processing are presented.

**Lecture Quality Management**  
Due to the digitization and networking of production as well as construction and the associated increase in data, quality management will play an increasingly important role in the future. The lecture "Quality Management" shows how this increase in data can be used in quality management. With this objective, mathematical-statistical basics as well as data analytics and AI methods will be presented which are used to generate information from the data. Then, it will be shown how these methods can be used for different areas of quality management, e.g., process control, risk management, customer needs, in order to finally offer decision support, possibly by employing smart devices.

**Lecture Production Metrology**  
Measurements are essential for the quality assurance of processes and products and become more and more relevant considering recent developments in the digitalization of production. The extraction of process information and knowledge is only possible with the application of the right measurement systems. The course Production Metrology offers you a deep insight in the world of metrology. You will learn about why it is necessary to measure, how to plan and execute measurements, what devices and techniques are available, and, of course, how to evaluate and interpret the results. Many practical and demonstrative exercises will enable you to use organizational and methodical measures for monitoring production processes and to assure the products’ fulfilment of previously defined requirements.


**Lectures:**  

- Mechanical Engineering Advanced:
  - Robotic Sensor Systems
  - Quality Management
  - Production Metrology





