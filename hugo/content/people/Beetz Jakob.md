+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/jakob_beetz.png"
type = "rwth_person"
weight = 4
title= "Univ.-Prof. Dr.<br />Jakob Beetz"

+++

#### Univ.-Prof. Dr. Jakob Beetz

Director of the Chair for Computational Design (CAAD),  
Faculty 2 of Architecture  
RWTH Aachen University  

Website: [https://dc.rwth-aachen.de/en](https://dc.rwth-aachen.de/en)  
Email: [j.beetz@caad.arch.rwth-aachen.de](mailto:j.beetz@caad.arch.rwth-aachen.de)

The design, planning, construction and operation of user-friendly and sustainable buildings and cities require the close cooperation among many.  

The efficient communication between stakeholders is a key factor for  the integration of knowledge and information of different disciplines and  domains. Next to traditional two-dimensional technical drawings, a wide  range of digital tools are increasingly being used across the whole lifecycle  of buildings. These tools are based on shared, interdisciplinary building  information models (BIM) that can be used for e.g. requirements  management, parametric design variations or the simulation of building  physics performance, structural behavior and construction processes.  

The study, understanding, application and development of these fundamental  methods, technologies and tools are the central aspects of the chair Design  Computation – CAAD (Computer Aided Architectural Design).  

**Lectures:**  

- Architecture Basics:
  - CR_Advanced Fundamentals of Building Information Modelling