+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/thomas_bergs.png"
type = "rwth_person"
weight = 25
title= "Univ.-Prof. Dr.-Ing.<br />Thomas Bergs"

+++

#### Univ.-Prof. Dr.-Ing. Thomas Bergs

Director of the Chair of Manufacturing Technology at the Laboratory for Machine Tools and Production Engoineering (WZL)  
Faculty 4 of Mechanical Engineering  
RWTH Aachen University


Website: [https://wzl.rwth-aachen.de](https://www.wzl.rwth-aachen.de/cms/~sijq/WZL/?lidx=1)  
Email: [t.bergs@wzl.rwth-aachen.de](mailto:T.Bergs@wzl.rwth-aachen.de)

The Chair of Manufacturing Technology at the Laboratory for Machine Tools and Production Engineering (WZL) of RWTH Aachen University under the direction of Professor Thomas Bergs teaches and conducts research in the fields of the fundamentals of manufacturing processes, process investigations of manufacturing processes, gear technology, process monitoring and simulation, and technology planning. Existing knowledge about the optimal use of manufacturing technologies is continuously challenged and deepened and new research approaches are developed.  

The chair carries out projects with companies as well as publicly funded projects in direct cooperation or in the form of technology working groups. In this way, research results can be applied to current problems in industrial production environments. For this purpose, the chair has among others access to an extensive, ultra-modern machine park at the Cluster Production Engineering.  

**Lectures:**  

- Mechanical Engineering Basic and Advanced:
  - Manufacturing Technology I
  - Manufacturing Technology II
  - Simulation Techniques in Manufacturing Technology
