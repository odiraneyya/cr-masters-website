+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/weiguo_xu.png"
image_caption = "/img/people/tsinghua.png"
type = "other_person"
weight = 96
title = "Univ.-Prof. Dr.-Ing. <br />Weiguo Xu "
+++

#### Univ.-Prof. Dr.-Ing. Weiguo Xu    

Professor and former Chair of Architecture Department / Director of the DADA,    
School of Architecture  
Tsinghua University  

   
Website: [https://www.tsinghua.edu.cn](https://www.tsinghua.edu.cn/en/index.htm)  
Email: [xwg@mail.tsinghua.edu.cn](mailto:xwg@mail.tsinghua.edu.cn)

Prof. Xu is a pioneer architect who has been active internationally in the field of digital design and digital fabrication for years. He has devoted many excellent researches and practical projects to the innovation of architecture and construction, such as robotic masonry system construction, concrete 3D printing, AI form finding, big data in architecture and etc. One of his prominent works – 3D printed concrete bridge is listed as the world’s longest 3D-printed concrete pedestrian bridge.  

Prof. Xu has published more than 140 papers and 17 books. A series of his research projects have been funded by NSFC. He has lectured worldwide, been visiting scholar at MIT in 2007, and taught in SCI-Arc and USC in 2011-2012. He has curated the DADA series events in 2013, and co-curated Architecture Biennial Beijing 2004, 2006, 2008 and 2010.
 

**Lectures:**

- Construction Robotics Lecture
