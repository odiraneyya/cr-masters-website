+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/markus_kuhnhenne.png"
image_caption = "/img/people/mlb_rwth.png"
type = "rwth_person"
weight = 62
title = "Univ.-Prof. Dr.-Ing.<br />Markus Kuhnhenne"
+++

#### Univ.-Prof. Dr.-Ing. Markus Kuhnhenne

Institute of Steel Construction, Sustainable Building Envelopes (MLB),  
Faculty 3 of Civil Engineering  
RWTH Aachen University  

Website: [https://stb.rwth-aachen.de](https://www.stb.rwth-aachen.de/cms/~iozv/STB/)  
Email: [mku@stb.rwth-aachen.de](mailto:mku@stb.rwth-aachen.de)

The professorship incorporates research and teaching, covering aspects of static design, building physics and, in particular, the sustainability of building envelopes made of lightweight metal structural elements. In addition, there are many interdisciplinary research topics based on classical steel construction and other related topics. Considering the sustainability of construction is an important requirement for future-proofing buildings. 

The advantages of lightweight metal construction include the high recycling potential, the easy deconstruction and reusability of individual components as well as the high durability and value of the components. In addition to discussing stability and mechanical strength, there is a focus on energy-efficient construction. The increase in the energy quality of lightweight metal building envelopes is through the reduction of heat transfer. In addition, it is becoming increasingly important to consider material and resource efficiency due to increasing production costs and the decreasing economic availability of raw materials. Future-orientated and sustainable construction means optimising the ecological, economic and social aspects of buildings. Teaching in this area combines design, planning and performance, along with assessing the field of lightweight metal construction and paying particular attention to its sustainability. 

**Lectures:**  

- Construction Robotics Lecture

