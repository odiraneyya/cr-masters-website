+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/jacqueline_lemm.png"
type = "rwth_person"
weight = 65
title= "Dr. phil. Jacqueline Lemm"
+++

#### Dr. phil. Jacqueline Lemm

Chair and Project Manager at the Chair of Sociology of Technology and Organization (STO),  
Faculty 7 of Arts and Humanities  
RWTH Aachen University 

Website: [https://www.soziologie.rwth-aachen.de](https://www.soziologie.rwth-aachen.de)  
Email: [jlemm@soziologie.rwth-aachen.de](mailto:jlemm@soziologie.rwth-aachen.de)

Human-robot collaboration represents the connection between the way humans and robots work. Humans and robots work on the same task, usually at the same workstation. They support each other with their abilities and thus complement each other to complete the task in the best possible way. In cooperation, man carries out the individual and sensitive tasks and the robot carries out the precise, dangerous and repetitive tasks. The work steps are separated from each other. Thus there are fixed intervention times of the robot, which are determined by the human being.  

The robot can reduce the susceptibility of humans to errors in production because it works more precisely and does not deviate from the programming. In addition, the robot increases safety, because it can take over tasks that are dangerous for humans. As a result, the robot enhances the occupational field, as this becomes safer and physically easier for humans. If, however, an error has crept into the work through programming or through technical failure, the human being can correct these errors, since he can think in a solution-oriented way. In addition, the human being contributes to individualization in the process.  

In human-robot collaboration, the robot thus serves as an assistance system for humans in order to increase the human radius of action. The focus is on relieving the human being. In this teaching unit, basic knowledge of human-robot collaboration is imparted from a relational technical-sociological perspective.

**Lectures:**  

- Construction Robotics lecture:
  - Human-robot collaboration from a sociological perspective