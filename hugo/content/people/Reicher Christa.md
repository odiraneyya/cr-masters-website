+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/christa_reicher.png"
type = "rwth_person"
weight = 90
title= "Univ.-Prof. Christa Reicher"
+++

#### Univ.-Prof. Christa Reicher

Director of the Chair of Urban Design and the Institute for Urban Design and European Urbanism  
Faculty 2 of Architecture  
RWTH Aachen University  

Website: [https://staedtebau.rwth-aachen.de](https://www.staedtebau.rwth-aachen.de)  
Email: [reicher@staedtebau.rwth-aachen.de](mailto:reicher@staedtebau.rwth-aachen.de)

The Chair of Urban Design and the Institute for Urban Design and European Urbanism are concerned with complex urban structures ranging from a regional and city-wide context, throughout the neighbourhood level, up to a single building. 

Based on historically grown structures, the focus is on design and further development of sustainable cities and neighbourhoods, taking into account their technical, ecological, economic, and socio-cultural framework conditions. The core in teaching and research are typological building blocks of the city and their genesis in terms of urban planning history as well as different methods of urban design and design principles.

The Chair and the Institute also lead the European master programme Transforming City Regions. The programme presents dynamics of urban areas in Europe, discusses in this context a new definition of the city, and elaborates contemporary processes shaping cities and urban areas. These dynamics of change play out in many different arenas including the physical and territorial, and are driven by demographic, socio-economic, environmental and political changes and developments. 

**Lectures:**  

- Architecture Advanced:
  - Urban Transformation



