+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/michael_vorlaender.png"
type = "rwth_person"
weight = 110
title= "Univ.-Prof. Dr.<br />Michael Vorländer"
+++

#### Univ.-Prof. Dr. Michael Vorländer

Chair and Institute of Technical Acoustics (ITA)  
Faculty 6 of Electrical Engineering and Information Technology  
RWTH Aachen University 

Website: [https://www.akustik.rwth-aachen.de](https://www.akustik.rwth-aachen.de)  
Email: [post@akustik.rwth-aachen.de](mailto:post@akustik.rwth-aachen.de)

The main research focus of the Chair of Technical Acoustics is “Auralization”. It is the technique of creation and reproduction of sound on the basis of computer data. With this tool is it possible to predict the character of sound signals which are generated at the source and modified by reinforcement, propagation and transmission in performance spaces, rooms in general, buildings in general, and urban environments, just to give a few examples.  

In teaching, we offer a comprehensive collection of the basics of sound and vibration, psychoacoustics, acoustic modelling, simulation, signal processing and audio reproduction. With some mathematical prerequisites, the main strategy of auralization can followed easily, and own implementations of auralization in various fields of application in architecture, acoustic engineering, sound design and virtual reality can be created. In basic research, the technique of auralization may be useful to create sound stimuli for specific investigations in linguistic, medical, neurological and psychological research, and in the field of human-machine interaction.


**Lectures:**  

- Computer Science Advanced:
  - Acoustic Virtual Reality 




