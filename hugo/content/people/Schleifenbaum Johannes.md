+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/johannes_schleifenbaum.png"
type = "rwth_person"
weight = 98
title= "Univ.-Prof. Dr.-Ing.<br />Dipl.-Wirt.Ing.<br />Johannes Henrich Schleifenbaum "
+++

#### Univ.-Prof. Dr.-Ing. Dipl.-Wirt.Ing. Johannes Henrich Schleifenbaum  

Director of the Chair for Digital Additive Production (DAP),  
Faculty 4 of Mechanical Engineering  
RWTH Aachen University  


Website: [https://dap-aachen.de](https://dap-aachen.de)  
Email: [johannes.henrich.schleifenbaum@dap...de](mailto:johannes.henrich.schleifenbaum@dap.rwth-aachen.de)

The RWTH Aachen University Chair for Digital Additive Production DAP researches fundamental technical and economic Additive Manufacturing interrelationships. From component design, to supply chain, production and component handling, to application properties of additively manufactured components: The Chair DAP considers all horizontal and vertical elements of the Additive Manufacturing process chain.  

Besides the further development of existing AM processes and existing machine and system technology especially the focus on software-driven end-to-end processes is an essential working point of the DAP. Starting with bionic lightweight construction, functional optimization for AM and the design of "digital the functional optimization for AM and the design of "digital  materials" up to the validation in the real process and the derivation of static and dynamic parameters, the advantage can be realized with additive processes using digital technologies. For this purpose, almost all common software suites in the field of Authoring systems (CAD) and commercially available CAx systems, FEM modelers etc. are available.  

On the machine side, both commercially available equipment as well as adapted laboratory systems and Experimental setups are ready for usage. Beyond the purely technological topics, the chair supports industrial partners in mastering the challenges posed by the implementation of Additive Manufacturing and the involved emerging complexity. By means of AM-driven consulting services, the DAP guarantees an integrated consideration of the implications of additive manufacturing at a strategic, tactical and operative company level and thus creates the conditions for the competitiveness of the numerous industrial partners in the AM environment within the scope of fundamental, joint and industrial projects from the various industries, as well as close cooperation with non-university research institutions, the DAP has an extensive expertise in the field of Additive Manufacturing and the supporting processes.


**Lectures:**  

- Mechanical Engineering Basics:
  - Additive Manufacturing I (AM I)
  - Additive Manufacturing II (AM II)





