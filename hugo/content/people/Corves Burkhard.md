+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/burkhard_corves.png"
type = "rwth_person"
weight = 36
title= "Univ.-Prof. Dr.-Ing. Dr. h. c.<br />Burkhard Corves"
+++

#### Univ.-Prof. Dr.-Ing. Dr. h. c. Burkhard Corves

Director and Chair of the Institute of Mechanism Theory, Machine Dynamics and Robotics (IGMR),  
Faculty 4 of Mechanical Engineering  
RWTH Aachen University  

Website: [https://igmr.rwth-aachen.de/.../](https://www.igmr.rwth-aachen.de/index.php/en/)  
Email: [corves@igmr.rwth-aachen.de](mailto:corves@igmr.rwth-aachen.de)

Mechanism Theory and Machine Dynamics are the foundation of proper motion design for machines and technical equipment in various application areas. This holds especially true when it comes to robotic applications especially in the area of handling, logistics, assembly and automation. In general, automation can be seen in many different application areas, but in traditional construction engineering automation is quite rare, especially when it comes together with the use and deployment of robotic solutions.  

The introduction of computer based tools such as the building information model (BIM) has led to new possibilities and opportunities in the planning, design and application of robotic solutions in the construction business. This includes both stationary robotic arms as well as mobile robots and also the combination of both. The lectures mentioned below will give the student the knowledge and capabilities of modelling, analyzing and optimizing robotic systems.

**Lectures:**  

- Mechanical Engineering Advanced:
   - Advanced Robotic Kinematics and Dynamics
   - Multibody Dynamics