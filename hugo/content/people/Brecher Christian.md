+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/christian_brecher.png"
type = "rwth_person"
weight = 24
title= "Univ.-Prof. Dr.-Ing.<br />Christian Brecher"
+++

#### Univ.-Prof. Dr.-Ing. Christian Brecher  

Director at the Chair of Machine Tools at the Laboratory for Machine Tools and Production Engineering (WZL),  
Faculty 4 of Mechanical Engineering  
RWTH Aachen University  

Website: [https://wzl.rwth-aachen.de](https://www.wzl.rwth-aachen.de/)  
Email: [c.brecher@wzl.rwth-aachen.de](mailto:c.brecher@wzl.rwth-aachen.de)

The lecture mechatronics and control technology provides hardware-and software-related knowledge base in the field of mechatronics and control of facilities for production. The control system decreases with increasing automation in the industry plays a key role. Along the different levels in automation and control technologies, mechatronic systems will be taught to the sensor/actuator level, the control level to the management level. The different types of controllers and systems, and their programming methods are covered here as well as drive types and procedures to process and condition monitoring of mechatronic systems.  

The course thus provides knowledge and experience in the field of construction, design and planning of mechatronic systems in production. In addition, students will be in a position to cross-border concepts of machine control, as well as the machinery and process monitoring and can apply this knowledge derived from assessments of the quality of industrial control solutions.

**Lectures:**

- Mechanical Engineering Basics:
  - Mechatronics and Control for Production Plants


