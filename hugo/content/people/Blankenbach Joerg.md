+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/joerg_blankenbach.png"
image_caption = "/img/people/gia_rwth.png"
type = "rwth_person"
weight = 28
title= "Univ.-Prof. Dr.-Ing.<br />Jörg Blankenbach"
+++

#### Univ.-Prof. Dr.-Ing. Jörg Blankenbach 

Director of Geodetic Institute and Chair for Computing in Civil Engineering & GIS (gia),  
Faculty 3 Faculty of Civil Engineering  
RWTH Aachen University  

Website: http://www.gia.rwth-aachen.de/  
Email: blankenbach@gia.rwth-aachen.de

As in many areas, digitization is also progressing rapidly in the construction industry. Its digital transformation is directly linked to Building Information Modeling (BIM) and web-based (distributed) Geographic Information Systems (GIS), which are based on two- and three-dimensional digital models of the (built) environment. Planning and operation both depend on the digital data acquisition and modeling of the as-is situation. Modern reality capturing methods utilize terrestrial and airborne (e.g. drones) geospatial sensor technology, succeeded by (partially) automated modeling processes. While satellite-based georeferencing techniques are used in uncovered environments, specialized indoor positioning methods are required for example inside buildings. Based on digital building models and precise localization techniques mobile mixed reality system can be realized. Research, development and application of these technologies and methods as well as transferring them into student education are central tasks of the gia.

**Lectures:**

- Construction Robotics Lecture
- Civil Engineering Advanced:
  - Building Information Modeling
