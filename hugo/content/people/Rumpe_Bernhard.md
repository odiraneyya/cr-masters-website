+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/bernhard_rumpe.png"
image_caption = "/img/people/se_rwth.png"
type = "rwth_person"
weight = 94
title = "Univ.-Prof. Dr.<br />Bernhard Rumpe"
+++

#### Univ.-Prof. Dr. Bernhard Rumpe

Director of Chair for Software Engineering (SE),   
Faculty 1 of Mathematics, Computer Science Department    
RWTH Aachen University  

Website: [https://www.se-rwth.de](https://www.se-rwth.de/)  
Email: [rumpe@se-rwth.de](mailto:rumpe@se-rwth.de)

The Software Engineering group concentrates on innovative and practically useful concepts, methods, and tools for the development of complex business and software-intensive embedded systems on a sound and reliable scientific and engineering basis. In the current age of digitalization, it is important to develop at high speed and deliver good quality.
In recent years, software engineering has significantly extended and consolidated its portfolio of methods, techniques, and tools to support development of highly critical, reliable, or functionally complex systems within the predefined time and a given budget while meeting the required quality. It is our goal to transfer this knowledge and the accompanying tools into industry as well as to customize and apply them to project and company specific needs.
Our mission: "To define, improve, and industrially apply techniques, concepts and methods for innovative and efficient development of software and digitalization-based systems, such that high-quality products can be developed in a shorter period of time and with flexible integration of changing requirements."


**Lectures:**  

- Computer Science Basics
  - Software Engineering
  - Model-Based Software Development
