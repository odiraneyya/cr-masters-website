+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/katharina_schmitz.jpg"
type = "rwth_person"
weight = 100
title= "Univ.-Prof. Dr.-Ing.<br />Katharina Schmitz"
+++

#### Univ.-Prof. Dr.-Ing. Katharina Schmitz 

Director the Institute for Fluid Power Drives and Systems (ifas),    
Faculty 4 of Mechanical Engineering  
RWTH Aachen University 

Website: [https://www.ifas.rwth-aachen.de](https://www.ifas.rwth-aachen.de)  
Email: [post@ifas.rwth-aachen.de](mailto:post@ifas.rwth-aachen.de)

The Institute for Fluid Power Drives and Systems (ifas) of RWTH Aachen University is one of the world’s largest and best known scientific institutions conducting research in all aspects of fluid power. This includes hydraulics and pneumatics, as well as all of its fields of application. To be equipped for the future, current research includes areas such as information technology, servo-control engineering, electrical engineering, tribology and chemistry on top of mechanical engineering.  

Environmental, safety and health regulations as well as increasing industrial consumer requirements necessitate a continuous development of sustainable and efficient fluid power technology. Greater environmental awareness and new technologies, e.g., mechatronic systems, preventative maintenance, additive manufacturing, biomedical applications, surface coating techniques and modern information technologies, offer new perspectives and fields of application for fluid power systems.  

The highly motivated team of aspiring young scientists takes on the challenges presented by this extensive and diversified field of study. The institute’s multiple and profound national and international connections with manufacturers and users of fluid power components and systems, as well as other research facilities, ensure that its activities are leading the way into the future of research, development and education of fluid power systems.

**Lectures:**  

- Mechanical Engineering Advanced:
  - Fundamentals of Fluid Power




