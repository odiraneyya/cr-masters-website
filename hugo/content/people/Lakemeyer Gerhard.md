+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/gerhard_lakemeyer.png"
type = "rwth_person"
weight = 64
title= "Univ.-Prof. Gerhard Lakemeyer"
+++

#### Univ.-Prof. Gerhard Lakemeyer 

Head of the Knowledge-Based Systems Group  
Faculty 1 of Mathematics, Computer Science Department  
RWTH Aachen University  

Website: [https://kbsg.rwth-aachen.de](https://www.kbsg.rwth-aachen.de/)  
Email: [gerhard@kbsg.rwth-aachen.de](mailto:gerhard@kbsg.rwth-aachen.de)

The Knowledge-Based Systems Group (KBSG) conducts research in Artificial
Intelligence, in particular, in the areas of Knowledge Representation and
Reasoning (KR&R) and Cognitive Robotics.  

A major focus of our work in KR&R is the
study of action formalisms based on the situation calculus, including the action
programming language Golog, which was originally developed at the University of
Toronto. The group is also involved in developing methods for the high-level
control of mobile robots, including task planning and execution monitoring.
One of the application areas is production logistics, where robots need to
interact with machines in the production of goods. In this context we are part
of the Carologistics Team, the five-time world champion of the Robocup Logistics
League competition.  

Finally, within the Excellence Cluster "Internet of Production" the
group is concerned with the integration of data-driven and model-based
approaches in Production Engineering.

**Lectures:**  

- Computer Science Basics:
  - Introduction to Artificial Intelligence
