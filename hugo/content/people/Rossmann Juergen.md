+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/juergen_rossman.png"
type = "rwth_person"
weight = 92
title= "Univ.-Prof. Dr.-Ing.<br />Jürgen Roßmann"
+++

#### Univ.-Prof. Dr.-Ing. Jürgen Roßmann

Director of Institute for Man-Machine Interaction,  
Faculty 6 of Electrical Engineering and Information Technology  
RWTH Aachen University 

Website: [https://mmi.rwth-aachen.de/en](https://www.mmi.rwth-aachen.de/en/)  
Email: [rossmann@mmi.rwth-aachen.de](mailto:rossmann@mmi.rwth-aachen.de) 

The main focus of the Institute for Man­Machine Interaction (MMI) is to bridge the gap between fields of robotics and virtual reality in order to develop, implement and apply new concepts of advanced control, man-machine interaction and communication. MMI specializes in modelling systems and environments, in analyzing complex systems, in connecting humans, devices and machines, and in realizing intelligent systems. MMI’s innovative Digital Twins revolutionize the development process and the operation of (not only) robotic systems.  

Virtual and real robotic systems are co-developed, simulated and controlled in the context of their respective work environments in Virtual Testbeds. The application areas span various industries and disciplines, ranging from robot manipulators, mobile robots, spacecraft and road vehicles up to agricultural, forestry, and construction machines.  


**Lectures:**

- Construction Robotics Lecture:
  - Simulation of Robotic Systems, Sensors, Environment, and Processes
