+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/stefan_kowalewski.png"
type = "rwth_person"
weight = 59
title= "Univ.-Prof. Dr.-Ing.<br />Stefan Kowalewski"

+++

#### Univ.-Prof. Dr.-Ing. Stefan Kowalewski  

Informatik 11 – Embedded Software,  
Faculty 1 of Mathematics, Computer Science Department  
RWTH Aachen University   

Website: [https://www.embedded.rwth-aachen.de](https://www.embedded.rwth-aachen.de)    
Email: [kowalewski@embedded.rwth-aachen.de](mailto:kowalewski@embedded.rwth-aachen.de)  

In virtually any domain of technology, software-intensive computer systems supervise and control the functionality and ensure the safety of products, machines, plants, and processes. In Computer Science, these systems are subsumed under the term “Embedded Systems”. Our research interest is in model-based, mathematically rigorous methods for their development. In teaching, we introduce the basic platforms and technology for realizing embedded systems and fundamental methods for their design and the validation of their intended functionality.


**Lectures:**  

- Computer Science Basics:
  - Embedded Systems
