+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/sven_klinkel.png"
type = "rwth_person"
weight = 57
title= "Univ.-Prof. Dr.-Ing. habil.<br />Sven Klinkel"

+++

#### Univ.-Prof. Dr.-Ing. habil. Sven Klinkel

Director of Chair of Structural Analysis and Dynamics,  
Faculty 3 of Civil Engineering  
RWTH Aachen University 

Website: [https://lbb.rwth-aachen.de/](https://www.lbb.rwth-aachen.de/cms/~eaxh/LBB/?lidx=1 )  
Email: [klinkel@lbb.rwth-aachen.de ](mailto:klinkel@lbb.rwth-aachen.de)

The Chair of Structural Analysis and Dynamics is characterized by the modeling of load-bearing structures and the development of mathematical calculation methods for structural analysis. The aim of the courses is to enable students to assess the load-bearing behavior of bar-, plate- and shell-like structures under static and dynamic load. The lectures teach methods for the analysis of internal forces, stress and deformation response. Modeling, model adaptivity, dimensionally adapted structural modeling, e.g. connecting and joining structures and the coupling of different types of structures play an important role.  

A further important component of teaching and research are computational methods for structural analysis, which are an indispensable part of structural simulation, especially in the context of earthquake engineering and vibration protection. Based on these fundamental methods it is possible to carry out sustainable and safe dimensioning of structures.

**Lectures:**  

- Civil Engineering Advanced:
  - Advanced Structural Analysis 
  - Structural Dynamics 
  - Plates and Shells

