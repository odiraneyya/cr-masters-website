+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/jerome_frisch.png"
type = "rwth_person"
weight = 42
title= "Dr.-Ing. Jérôme Frisch"
+++

#### Dr.-Ing. Jérôme Frisch 

Chief Engineer of Institute of Energy Efficiency and Sustainable Building (E3D),  
Faculty 3 of Civil Engineering  
RWTH Aachen University 
 	
Website: [https://www.e3d.rwth-aachen.de/en](https://www.e3d.rwth-aachen.de/en)  
Email: [frisch@e3d.rwth-aachen.de](mailto:frisch@e3d.rwth-aachen.de)


Building Performance Simulation (BPS) can be used to analyze and investigate a building’s response to specific boundary conditions or a change in locality without the necessity of constructing the building in the real world. It can be used in early design stages to evaluate the performance of different design choices or in later stages to optimize building operation. In this course, the basics of mathematical-physical models and numerical methods will be investigated in a lecture and a practical part.   

The following topics are discussed in detail within the course:  basic principles of planning and implementation of energy performance modeling and simulation, thermodynamic and multidimensional effects, solar modeling and ray tracing algorithms, and practical implementation of a multi zonal building thermal simulation using Modelica.  


**Lectures:**  

- Civil Engineering Advanced:
  - Building Performance Simulation


