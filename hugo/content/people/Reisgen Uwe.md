+++
date = "2020-03-30T09:00:00+02:00"
image = "/img/people/uwe_reisgen.jpg"
type = "rwth_person"
weight = 88
title= "Univ.-Prof. Dr.-Ing.<br />Uwe Reisgen"
+++

#### Univ.-Prof. Dr.-Ing. Uwe Reisgen

Director of Welding and Joining Institute (ISF),  
Faculty 4 of Mechanical Engineering  
RWTH Aachen University 

Website: [https://www.isf.rwth-aachen.de](https://www.isf.rwth-aachen.de)  
Email: [office@isf.rwth-aachen.de](mailto:office@isf.rwth-aachen.de)

Joining in general is one of the key-processes for most production processes. This also applies to the construction sites of civil engineering. Automated production including joining processes such as welding or adhesive bonding require a fundamental understanding of their function principles, their impact on construction materials as well as the prerequisites for their application.  

The Welding and Joining Institute (ISF) of the RWTH Aachen University has, for almost 60 years now, been dealing with modern welding and joining technologies, including arc-welding and adhesive bonding as they are widely used on construction sites. Main emphasis of research and teaching is put on the application-oriented implementation for innovative materials and design. Furthermore ISF offers a wide range of topics of bachelor and master thesis in the field of welding and joining.

**Lectures:**  

- Mechanical Engineering Basics:
  - Joining Technology I - Basic Course


