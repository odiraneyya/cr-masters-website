﻿+++
weight = 10
+++

{{<image image="/img/contact/Mood-Banner_contact.jpg" opyright="IP, RWTH Aachen">}}

{{<inlinehead title="Contact">}}

The Master’s programme is coordinated by the Chair of Individualized Production at the Faculty of Architecture of RWTH Aachen University. For any questions please use the contact below:

Chair of Individualized Production  
RWTH Aachen University  
Schinkelstr. 1  
52062 Aachen, Germany

Email: 	 cr@ip.rwth-aachen.de  
Phone: +49 241 80 95005
