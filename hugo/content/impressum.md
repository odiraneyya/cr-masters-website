+++
date = "2016-09-20T11:38:51+02:00"
title = "Impressum"
type = "single-page"
+++

## Notice concerning the party responsible for this website
Chair of Individualized Production  
RWTH Aachen University  
Schinkelstr. 1  
52062 Aachen, Germany

Email: 	 cr@ip.rwth-aachen.de  
Phone: +49 241 80 95005

Represented by Sigrid Brell-Cokcan and Elisa Lublasser

## General notes
The operator of this website takes the protection of your personal data very seriously. We treat your personal data as confidential and in accordance with the statutory data protection regulations and the RWTH privacy policy. Please note the [disclaimer for the website by RWTH Aachen University](https://www.rwth-aachen.de/cms/root/Footer/Services/~cesv/Datenschutzerklaerung/?lidx=1).

Suggestions and wishes are always welcome. Please address them to Elisa Lublasser using the CR email address above. 

The reproduction of content from this website is permitted. A complete indication of the source is required. Specimen copies requested.


