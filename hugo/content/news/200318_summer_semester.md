+++
image = "/img/news/summer_semester_2020.png"
date = 2020-03-18T09:00:00+02:00
title = "Important Updates about 2020 Summer Semester"
+++

Due to the Corona virus, RWTH University has released regulations to secure the health of RWTH students and staffs. According to the regulations, we would like to inform you about some updates of the coming 2020 Summer Semester.

The 2020 Summer Semester for Construction & Robotics students will start from 6th of April. However, we will provide all the lectures and workshops only in DIGITAL form, such as online courses, digital scripts, Email consultancy and so on. Only from 20th of April, the offline courses are possibly available. Please pay attention to the information in your RWTH Moodle ([https://moodle.rwth-aachen.de/](https://moodle.rwth-aachen.de/)). Detailed information about the new semester courses and the instruction on course selection will be uploaded onto Moodle at 27th of March. You can start to register courses from 2nd of April. All the learning materials will be in Moodle as well. 

If there are any questions, please write Email to [cr@ip.rwth-aachen.de](mailto:cr@ip.rwth-aachen.de).