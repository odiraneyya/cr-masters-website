+++
date = "2020-04-07T09:00:00+02:00"
title = "Reference Construction Site - Important Infrastructure for CR Master"
image = "/img/news/ReferenceSiteForCR.jpg"
+++

The reference construction site of the Center Construction Robotics (CCR) on the Campus West covers 10,000 square meter. It serves as a living lab for on-site research and teaching in close proximity to the RWTH Aachen. As the most important infrastructure of Master Construction and Robotics, it is used for practice-oriented and research-led knowledge transfer.  

Research is being conducted here at the construction site of the future via digitalization – from the pre-production of building elements to the automated construction site. New construction processes and products, networked machines, the implementation of robots, software solutions, as well as teaching, working and communication concepts will be tested under real construction site conditions.
