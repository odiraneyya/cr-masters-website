+++
image = "/img/news/call_for_submissions.jpg"
date = 2019-10-22T11:16:39+00:00
title = "Call for submissions"
affiliatelink = "https://construction-robotics.eu/"
+++

## IMPORTANT DATES:  

> | | |
> | --------------------------------- | -------------------------: |
> | *Manuscript Submission:*          | Monday, December 15, 2019 |
> | *Decision by:*                    | January 15, 2020 |
> | *Reviews Submitted to Authors:*   | February 15, 2020 |
> | *Revised Manuscripts:*            | March 15, 2020 |
> | *Publication Date:*               | April 30, 2020 |  

***

**From Craft to Construction – Transitioning from the experimental scale to industrial production**  

This issue of the _Journal on Construction Robotics_ examines the ways in which experimental research impacts the construction site. By examining the connection (and gaps) between experimental research and industrial-scale construction, this issue examines the current and future role of robotics in the AEC industry.  

This issue seeks articles which detail the transfer of technology from academic/experimental research to architecture, engineering and construction. To advance the field of Construction Robotics, we seek to share projects where experiments are maturing into on site methods. We are seeking critical inquiries which examine the space between current experimental investigations and the path to construction level application, highlighting a way forward. This can include an analysis of technology needing further development before industrial implementation occurs.  

This call for articles seeks to expand the community of construction robotics past the traditional divide between research and practice. We are seeking technical design/engineering research articles which document and demonstrate that the lessons learned from digital craftsmanship have the power to impact the construction industry in real ways. From partnerships between industry and academia to architectural/engineering practices engaged in robotic development, this issue of the Journal on Construction Robotics has a special focus on how the creative robotics research community brings new levels of innovation into the industries of architecture, engineering and construction.  

***

## SCOPE OF ISSUE:  

* Automated Construction Technology  
  - Research in Automated Construction Technology should be presented which examines how experimental innovation matures into industrial application 
* Process Analysis  
  - The scope of the submitted papers should analyze existing processes and develop new methods, techniques and technologies for automation in construction.
* Sensors  
  - Submitted papers should detail research which integrates robots into construction through sensor systems which monitor changing conditions, from material to dynamic work environments.
* Adaptivity  
  - Research should be presented in which sensor data informs adaptive robotic processes so that workflows become reinformed with process information to increase efficiency and quality.
* On Site / Mobile / Collaborative Robots  
  - Submitted papers should document how, as robots move from pre fabrication to in situ construction, new systems for mobility and human machine interfaces are developed.

***


