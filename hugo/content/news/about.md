+++
image = "/img/home/CR_News_01.jpg"
date = 2019-10-02T11:16:39+00:00
title = "Application for summer term 2020"
+++

Great news: the CR master programme officially starts in summer term 2020! 
If you want to be one of the first students exploring the master's new opportunities, you can already apply at the end of this year.
The application period for EU students for the 2020 summer semester will begin December 2, 2019. Non-EU citizens can apply for the 2020/2021 summer semester until January 15, 2020.


