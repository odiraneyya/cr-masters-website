+++
date = "2020-07-06T09:00:00+00:00"
title = "CR Master - Design Driven Project"
image = "/img/news/ConstructionAndRobotics-DesignDrivenProject.png"
+++

We are happy to announce the final presentation of our first year students of the new international Master of Construction & Robotics (CR). We welcome our top-class industry jury who represent the board members of the CR programme and our premium partners within RWTH Aachen Center of Construction Robotics to help choose the first student projects to be prototyped on the reference construction site on RWTH Aachen Campus West during this summer.

It is our pleasure to start coming back to normal life in our physical environment and to meet our CR students for the first time in person after a full digital semester! We are looking forward to great presentations tomorrow, good luck to everybody!

