This repo has been moved from the RWTH GitLab website to the "gitlab.com", in order to support continuous deployment through "netlify.com".

# Editing the Website

To test simple changes on the IP website material. Please log into GitLab using the "Guest Editor (IP)" account.

After logging in to GitLab and opening this repository, create a new branch, and change what needs to be changed inside of that branch.

# How to Preview the Changes I Made?

After making changes inside the branch (such as adding a markdown or image files), you can preview the changes by hovering your mouse
over "CI / CD" from the left pane and choosing "Pipelines":

<img src="hugo/docs/docs_how-to-preview-deployed-branches.jpg" width="502px">

# Last Deployment Status

Sometimes, your changes will result in a pipeline that fails (rather than passes as shown in the image).

In 99% of the cases, this is due to a typo in the markdown file.

# What if I am Done Making Changes?

From the left pane, click on "Merge Requests", then create a new merge request explaining what you did, and asking for the changes to
be incorporated into the main copy of the website.

[![Netlify Status](https://api.netlify.com/api/v1/badges/dbc67a65-b975-48ba-836d-b80466ec89d2/deploy-status)](https://app.netlify.com/sites/nifty-payne-400c29/deploys)